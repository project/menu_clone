<?php

/**
 * Create the admin form.
 * 
 * @param form_state  reference to the state of the form
 */
function clone_sync_admin_form(&$form_state) {
  $form['clone_sync_general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );
  $form['clone_sync_general']['clone_sync_show_src_warnings'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display source warnings'),
    '#default_value' => variable_get('clone_sync_show_src_warnings', 1),
    '#description' => t('Display warning messages on source menus, informing the user that changes will be synced with the clone menu.'),
  );
  $form['clone_sync_general']['clone_sync_show_src_edit_warnings'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display source edit warnings'),
    '#default_value' => variable_get('clone_sync_show_src_edit_warnings', 1),
    '#description' => t('Display warning messages when editing an item of a souce menu, informing the user that changes will be synced with the clone menu.'),
  );
  $form['clone_sync_general']['clone_sync_show_dst_warnings'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display clone warnings'),
    '#default_value' => variable_get('clone_sync_show_dst_warnings', 1),
    '#description' => t('Display warning messages on clones of menus, informing the user that changes will be overwritten by the source menu.'),
  );
  $form['clone_sync_general']['clone_sync_show_dst_edit_warnings'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display clone edit warnings'),
    '#default_value' => variable_get('clone_sync_show_dst_edit_warnings', 1),
    '#description' => t('Display warning messages when editing an item of a clone menu, informing the user that changes will be overwritten by the source menu.'),
  );
  
  $form['clone_sync_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Synchronisation settings'),
  );
  $form['clone_sync_settings']['clone_sync_sync_menu_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sync menu title'),
    '#default_value' => variable_get('clone_sync_sync_menu_title', 1),
    '#description' => t("Synchronise changes in the souce menu's title name with its clone."),
  );
  $form['clone_sync_settings']['clone_sync_sync_menu_description'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sync menu description'),
    '#default_value' => variable_get('clone_sync_sync_menu_description', 1),
    '#description' => t("Synchronise changes in the souce menu's description name with its clone."),
  );
  $form['clone_sync_settings']['clone_sync_sync_menu_item_path'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sync menu item path'),
    '#default_value' => variable_get('clone_sync_sync_menu_item_path', 1),
    '#description' => t("Synchronise changes to the path of a menu item with its clone."),
  );
  $form['clone_sync_settings']['clone_sync_sync_menu_item_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sync menu item title'),
    '#default_value' => variable_get('clone_sync_sync_menu_item_title', 1),
    '#description' => t("Synchronise changes in the title of a menu item with its clone."),
  );
  $form['clone_sync_settings']['clone_sync_sync_menu_item_description'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sync menu item description'),
    '#default_value' => variable_get('clone_sync_sync_menu_item_description', 1),
    '#description' => t("Synchronise changes to the description of a menu item with its clone."),
  );
  $form['clone_sync_settings']['clone_sync_sync_menu_item_properties'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sync menu item properties'),
    '#default_value' => variable_get('clone_sync_sync_menu_item_properties', 1),
    '#description' => t("Synchronise changes to the properties of a menu item with its clone. Properties are: <strong>Enabled</strong>, <strong>Expanded</strong>, <strong>Parent item</strong>, <strong>Weight</strong>."),
  );

  return system_settings_form($form);
}

/**
 * Validation function for clone_sync_admin_form().
 * 
 * @param form       the form object
 * @param form_state reference to the form state
 * @see              clone_sync_admin_form()
 */
function clone_sync_admin_form_validate($form, &$form_state) {
  /*$menu = $form_state['values'];
  if (preg_match('/[^a-z0-9-]/', $menu['menu_name'])) {
    form_set_error('menu_name', t('The menu name may only consist of lowercase letters, numbers, and hyphens.'));
  }
  $sql = "SELECT menu_name FROM {menu_custom} WHERE menu_name = '%s'";
  $result = db_result(db_query($sql, 'menu-' . $menu['menu_name']));
  if (!empty($result)) {
    form_set_error('menu_name', t("The machine-readable name '@menu_name' must be unique. A menu named '@menu_name' already exists.", array('@menu_name' => $menu['menu_name'])));
  }*/
}

/**
 * Helper function for hook_form_[form_id]_alter().
 * Extends Menu clone module to add Clone sync settings.
 * 
 * @param form       reference to the form object
 * @param form_state the form state
 * @see              menu_clone_clone_form()
 */
function _clone_sync_menu_clone_clone_form_alter(&$form, $form_state) {
  // Shift submit button.
  $weight = isset($form['submit']['#weight']) ? $form['submit']['#weight'] : 0;
  $form['submit']['#weight'] = $weight + 1;
  $form['clone_sync'] = array(
    '#type' => 'fieldset',
    '#title' => t('Clone sync'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form['clone_sync']['menu_sync'] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep in sync'),
    '#default_value' => 0,
    '#description' => t('Tick this box if you wish to keep this clone in sync with its source menu.'),
    '#required' => FALSE,
  );
  $form['#submit'][] = '_clone_sync_menu_clone_clone_form_submit';
}

/**
 * Extra submit handler for menu_clone_clone_form().
 *
 * @param form       reference to the form object
 * @param form_state the form state
 * @see   _clone_sync_menu_clone_clone_form_alter()
 * 
 * TODO: serialise real data here.
 */
function _clone_sync_menu_clone_clone_form_submit(&$form, $form_state) {
  if ($form_state['values']['clone_sync']['menu_sync']) {
    $menu = $form_state['values'];
    $mappings = serialize(array());
    $sql = "INSERT INTO {clone_sync_relations} VALUES ('%s','%s','%s',%d,%d)";
    db_query($sql, $menu['menu_base']['menu_source'], 'menu-' . $menu['menu_base']['menu_name'], $mappings, 0, time());
  }
}

/**
 * Helper function for hook_form_[form_id]_alter().
 * Extends core menu delete functionality to also erase a menu from the
 * clone_sync_relations table.
 * 
 * @param form       reference to the form object
 * @param form_state the form state
 * @see              _clone_sync_menu_delete_menu_confirm_submit()
 */
function _clone_sync_menu_delete_menu_confirm_alter(&$form, $form_state) {
  $form['#submit'][] = '_clone_sync_menu_delete_menu_confirm_submit';
}

/**
 * Extra submit handler for menu_delete_menu_confirm().
 *
 * @param form       the form object
 * @param form_state reference to the form state
 * @see              _clone_sync_menu_delete_menu_confirm_alter()
 */
function _clone_sync_menu_delete_menu_confirm_submit(&$form, $form_state) {
  $sql = "DELETE FROM {clone_sync_relations} WHERE dst_menu = '%s'";
  db_query($sql, $form['#menu']['menu_name']);
}

/**
 * Helper function for hook_form_[form_id]_alter().
 * Extends core menu overview page to display a Clone sync message and to save
 * data to the clone_sync_relations table.
 * 
 * @param form       the form object
 * @param form_state reference to the form state
 * @see              _clone_sync_menu_overview_form_submit()
 * @see              menu_overview_form()
 */
function _clone_sync_menu_overview_form_alter(&$form, $form_state) {
  module_load_include('inc', 'clone_sync', 'includes/clone_sync.functions');
  $warnings = _get_warning_message($form['#menu']['menu_name']);
  // Warn the user we're watching this menu.
  if ($warnings['show_message']) {
    drupal_set_message($warnings['message'], 'warning');
    if ($warnings['resync']) {
      // Added so we can check if we need to update clone_sync_relations table.
      $form['clone_sync_resync'] = array(
        '#type' => 'value',
        '#value' => 'resync',
      );
      $form['#submit'][] = '_clone_sync_menu_overview_form_submit';
    }
  }
}

/**
 * Extra submit handler for menu_overview_form().
 * 
 * @param form       the form object
 * @param form_state reference to the form state
 * @see              _clone_sync_menu_overview_form_alter()
 */
function _clone_sync_menu_overview_form_submit(&$form, $form_state) {
  if (isset($form_state['values']['clone_sync_resync'])) {
    if ($form_state['values']['clone_sync_resync'] == 'resync') {
      $sql = "UPDATE {clone_sync_relations} SET resync = %d WHERE src_menu = '%s'";
      db_query($sql, 1, $form['#menu']['menu_name']);
    }
  }
}

/**
 * Helper function for hook_form_[form_id]_alter().
 * Extends core edit menu page to display a Clone sync message and to save
 * data to the clone_sync_relations table.
 * 
 * @param form       the form object
 * @param form_state reference to the form state
 * @see              _clone_sync_menu_edit_menu_submit()
 * @see              menu_edit_menu()
 * 
 */
function _clone_sync_menu_edit_menu_alter(&$form, $form_state) {
  module_load_include('inc', 'clone_sync', 'includes/clone_sync.functions');
  $warnings = _get_warning_message($form['menu_name']['#value'], 'edit');
  // Warn the user we're watching this menu.
  if ($warnings['show_message']) {
    drupal_set_message($warnings['message'], 'warning');
    if ($warnings['resync']) {
      // Added so we can check if we need to update clone_sync_relations table.
      $form['clone_sync_menu_name'] = array(
        '#type' => 'value',
        '#value' => $warnings['menu_name'],
      );
      $form['#submit'][] = '_clone_sync_menu_edit_menu_submit';
    }
  }
}

/**
 * Extra submit handler for menu_edit_menu().
 * 
 * @param form       the form object
 * @param form_state reference to the form state
 * @see              _clone_sync_menu_edit_menu_alter()
 */
function _clone_sync_menu_edit_menu_submit(&$form, $form_state) {
  if (isset($form_state['values']['clone_sync_menu_name'])) {
    $sql_extra = '';
    if (variable_get('clone_sync_sync_menu_title', 1)) {
      $sql_extra = "title = '".$form_state['values']['title']."'";
    }
    if (variable_get('clone_sync_sync_menu_description', 1)) {
      if ($sql_extra != '') {
        $sql_extra .= ', ';
      }
      $sql_extra .= "description = '".$form_state['values']['description']."'";
    }
    // Upate the menu's clone.
    if ($sql_extra != '') {
      $sql = "UPDATE {menu_custom} SET $sql_extra WHERE menu_name = '%s'";
      db_query($sql, $form_state['values']['clone_sync_menu_name']);
    }
    // Update the core Navigation menu entry.
    $path = 'admin/build/menu-customize/';
    if (variable_get('clone_sync_sync_menu_title', 1)) {
      $result = db_query("SELECT mlid FROM {menu_links} WHERE link_path = '%s'", $path . $form_state['values']['clone_sync_menu_name']);
      while ($m = db_fetch_array($result)) {
        $link = menu_link_load($m['mlid']);
        $link['link_title'] = $form_state['values']['title'];
        menu_link_save($link);
      }
    }
  }
}
/**
 * Helper function for hook_form_[form_id]_alter().
 * Extends core menu edit page to display a Clone sync message and to save
 * data to the clone_sync_relations table.
 * 
 * @param form       the form object
 * @param form_state reference to the form state
 * @see              _clone_sync_menu_edit_item_submit()
 * @see              menu_edit_item()
 */
function _clone_sync_menu_edit_item_alter(&$form, $form_state) {
  $form['#submit'][] = '_clone_sync_menu_edit_item_submit';
}

/**
 * Extra submit handler for menu_edit_item().
 * 
 * @param form       the form object
 * @param form_state reference to the form state
 * @see              _clone_sync_menu_edit_item_alter()
 */
function _clone_sync_menu_edit_item_item(&$form, $form_state) {
    dpm($form_state);
}