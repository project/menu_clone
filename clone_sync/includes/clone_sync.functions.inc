<?php

/*
 * Function to retrieve warning message information.
 * 
 * @param  menu_name machine readable menu name
 * @param  action    'edit' is the only possible argument here, defaults to ''
 * @return associative array containing 'menu_name', 'message', 'show_message'
 *         and 'resync'
 * @see   _clone_sync_menu_overview_form_alter()
 */
function _get_warning_message($menu_name, $action = '') {
  $warnings = array();
  $sql = "SELECT src_menu, dst_menu, last_sync FROM {clone_sync_relations} WHERE %s = '%s'";
  if ($action != '') {
    $action = '_'.$action;
  }

  // Check if on clone of a menu.
  $result = db_fetch_array(db_query($sql, 'dst_menu', $menu_name));
  if (!empty($result)) {
    $warnings['menu_name'] = $result['src_menu'];
    $warnings['message'] = t('This clone is being watched by <em><a href="@clone_sync">Clone sync</a></em>. Changes to this menu will be overwritten by its source <em><a href="@link">@source</a></em> when this menu is resynced.', array('@clone_sync' => url('admin/settings/clone_sync'), '@source' => $result['src_menu'], '@link' => url('admin/build/menu-customize/'.$result['src_menu'])));
    $warnings['show_message'] = variable_get('clone_sync_show_dst'.$action.'_warnings', 1);
    $warnings['resync'] = FALSE;
  }
  else {
    // Check if on source menu.
    $result = db_fetch_array(db_query($sql, 'src_menu', $menu_name));
    if (!empty($result)) {
      $warnings['menu_name'] = $result['dst_menu'];
      $warnings['message'] = t('This menu is being watched by <em><a href="@clone_sync">Clone sync</a></em>. Changes to this menu will be synced with its clone <em><a href="@link">@clone</a></em>.', array('@clone_sync' => url('admin/settings/clone_sync'), '@clone' => $result['dst_menu'], '@link' => url('admin/build/menu-customize/'.$result['dst_menu'])));
      $warnings['show_message'] = variable_get('clone_sync_show_src'.$action.'_warnings', 1);
      $warnings['resync'] = TRUE;
    }
    else {
      // Menu is not being synced.
      $warnings['menu_name'] = NULL;
      $warnings['message'] = NULL;
      $warnings['show_message'] = 0;
      $warnings['resync'] = FALSE;
    }
  }
  if ($warnings['show_message']) {
    $warnings['message'] = '<p><strong>' . $warnings['message'] . '</strong><br />' . t('Last synced on: @synctime', array('@synctime' => $result['last_sync'])) . '</p>';
  }
  return $warnings;
}